import undetected_chromedriver as uc
import newspaper
from newspaper import Article
#from newspaper import Config
from sumy.utils import get_stop_words
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import textrazor
import re
import sys,os
import datetime
from datetime import datetime
from selenium import webdriver
import nltk
nltk.download('punkt_tab')


import requests
import getpass
from collections import deque
import itertools

import webbrowser

script_dir="/home/z0/aggregate-jekyll"
venv_bin_dir="/home/z0/aggregate-jekyll/venv/bin"
jekyll_post_dir="/home/z0/cbdinfo/_posts"
textrazor_api_key="/home/z0/.textrazorapi.key"
firefox = webbrowser.Mozilla("/usr/bin/firefox") 
  

# read from file outside git dir
# GET TEXTRAZOR API KEY
with open(textrazor_api_key) as file:
    textrazor.api_key = file.read().replace('\n',' ')

# /home/user/oa_auth: 'email@email.email'
user = getpass.getuser()
auth = f'/home/{user}/oa_auth'
with open(auth) as file:
    email = file.read().replace('\n','')

def chromedriver():
    # CHROME CONFIG
    options = {
        "profile.default_content_setting_values": {
            "images": 2,
            "javascript": 2,
        }
    }
    options = Options()
    driver = uc.Chrome(options=options) 
    driver.get(url)
    page_source = driver.page_source
    # WRITE PAGE SOURCE TO _html.html
    x = open("_html.html", "w")
    x.write(page_source)
    x.close()
    driver.quit()
    #return page_source

def newspaper4k():
    # LOAD PAGE SOURCE INTO NEWSPAPER4K
    article = newspaper.article(url= 'file:///home/z0/aggregate-jekyll/_html.html')
    #article = newspaper.article('https://edition.cnn.com/2023/10/29/sport/nfl-week-8-how-to-watch-spt-intl/index.html')
    article.download()
    article.parse()
    
    d = dict([
        ('title',       article.title),
        #('authors',     ' | '.join(article.authors)),
        ('url',         url),
        ('text',        article.text)
    ])
    #print(article.authors)
    return d

def textrazor1():
    # CONFIG
    client = textrazor.TextRazor(extractors=["topics"])
    #client.set_cleanup_mode("cleanHTML")
    client.set_classifiers(["textrazor_newscodes"])
    response = client.analyze(str(text))

    topicx = []
    for topic in response.topics():
        if topic.score == 1:
            #print("validate iteration")
            #print(topic.label)
            topicx.append(topic.label.replace(',', ""))
    return topicx
#print(textrazor1())

def summarize():
    # SUMMARY OPTIONS
    LANGUAGE = "english"
    SENTENCES_COUNT = 5
    #nltk.download('punkt') 
    # CLEAN TEXT
    articletext_join_lines = "".join(text.replace("\n", " ").replace('"', ""))
    #print(article.text)
    parser = PlaintextParser.from_string(articletext_join_lines, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    article_summary = []
    for sentence in summarizer(parser.document, SENTENCES_COUNT):
        article_summary.append(str(sentence))

    clean_summary = ' '.join([str(elem) for elem in article_summary])
    return clean_summary

def create_post():
    print(title)
    #print(authors)
    #print(url)
    #print(text)
    mod_url2 = re.sub(r"[^a-zA-Z0-9]+", ' ', title).strip().casefold().replace(" ", "-")
    mod_tags = ','.join(textrazor1())
    datex = datetime.now()
    datex_fn = datex.strftime("%Y-%m-%d")
    mod_filename = ''.join((datex_fn, "-", mod_url2))
    _post = ''.join((jekyll_post_dir, '/', mod_filename, '.md'))
    
    input_writepost = input("Write post? Y/n ")

    if input_writepost.lower() == "y":
        summary = summarize()
        _post_content = ''.join(("---\nlayout: post\ntitle: \"", title, "\"\ndate: ", datex_fn, "\ntags: [",  mod_tags, "]\nauthor: ", authors, "\npubdate: \"", pubdate,"\"\ninstitutions: ", institutions, "\ndoi: \"", url, "\"\n---\n\n\n", summary))
        open(_post, 'w').close()
        d = open(_post, "w")
        d.write(_post_content)
        d.close()

def tail(fname, N):
    with open(fname) as file:
        # iterate over last N lines
        for line in (file.readlines() [-N:]):
            print(line, end ='')

# dataset
endpoint = 'works'
# results per page
rpp = 6
# pages to process
ptp = 50
# search filters. open access only, keywords
filters = 'from_publication_date:2020-01-01,language:en,is_oa:true,title.search:("cannabinoid" OR "cannabis" OR "hemp" OR "Cannabaceae" OR "THC" OR "CBD" OR "Cannabidiol" OR "Tetrahydrocannabinol" OR "Endocannabinoid")'

# TODO: VERIFY ALL RESULTS ARE OPENACCESS


# TODO: what happens when the end of the pages to process cycle iends?
#       it needs to loop to the next page, not through a number of pages
#       OR just make the number of ptp really high

N = 3
try:
    print("Auto Saves:")
    tail('_cursor_save_auto', N)
    print("Manual Saves:")
    tail('_cursor_save_manual', N)
except:
    print('File not found')


cursor = input("Input Cursor:")

# for each 'page' (idx) in 'pages to process' (ptp), keeping track of iterations (itr)
for idx, itr in enumerate(range(ptp)):
    #print("PAGE: " + str(itr))
    # api url
    oa_api_url = f'https://api.openalex.org/{endpoint}?sort=publication_year:asc&per-page={rpp}&filter={filters}&mailto={email}&cursor={cursor}'
    print("PROCESSING: " + oa_api_url + "\n")
    # serialize
    cannabinoid_data = requests.get(oa_api_url).json()
    # retrieve cursor (next page id)
    cursor = cannabinoid_data['meta'].get('next_cursor')
    now = datetime.now()
    dt_string = now.strftime("[%d-%m-%Y][%H:%M:%S]")

    csa = open("_cursor_save_auto", "a")
    csa.write(dt_string + '[' + cursor + ']' + "\n")
    print("Saved " + cursor)
    csa.close()
    #print(cursor)
    
    # for each result in current page
    for result in range(rpp):
        # collect data
        pubdate = cannabinoid_data['results'][result].get('publication_date')
        dtitle = cannabinoid_data['results'][result].get('display_name')
        url = cannabinoid_data['results'][result].get('doi')
        pmcid = cannabinoid_data['results'][result]['ids'].get('pmcid')
        pdf_url = cannabinoid_data['results'][result]['primary_location'].get('pdf_url')
        #source_name = cannabinoid_data['results'][result]['primary_location']['source'].get('display_name')
        author_count = len(cannabinoid_data['results'][result]['authorships'])

        author_list = []
        author_id_list = []


        #print("PUBLICATION DATE: " + str(pubdate))
        print("\nTITLE: " + str(dtitle) + "\n")
        #print("DOI: " + str(url))
        print("PMCID: " + str(pmcid))
        #print("PDFURL: " + str(pdf_url))
        #print("SOURCENAME: " + str(source_name))
        #print("AUTHORCOUNT: " + str(author_count))
        
        for result_author in range(author_count):
            author = cannabinoid_data['results'][result]['authorships'][result_author]['author'].get('display_name')
            author_id = cannabinoid_data['results'][result]['authorships'][result_author]['author'].get('id')

            #print(author)
            author_list.append(author)
            #author_id_list.append(author_id)

            institution_count = len(cannabinoid_data['results'][result]['authorships'][result_author]['institutions'])
            institution_list = []
            for result_institution in range(institution_count):
                institution = cannabinoid_data['results'][result]['authorships'][result_author]['institutions'][result_institution].get('display_name')
                institution_list.append(institution)
            institutions = ' | '.join(institution_list)
 
            #countries_count = len(cannabinoid_data['results'][result]['authorships'][result_author]['countries'])
            #for result_country in range(countries_count):
            #    country = cannabinoid_data['results'][result]['authorships'][result_author]['countries'][result_country].get(result_country)

            #author_dict = dict([
            #    ('author_name',     author),
            #    ('author_id',       author_id),
            #    ('institution',     institution)
            #    ('country',         country)
            #])
            #print("AUTHORDICT:")
            #print(author_dict)
            #author_string = str(author_dict.get('author_name') + 
                
        authors = ' | '.join(author_list)
        #print("AUTHORS: " + authors)
        #print("AUTHORS: " + ' | '.join(author_list))

        # TODO: store author info in array/dict
        # should firefox handle inital links until a proper mineable source is found?

        input_queue = input("Open DOI (o) | Open PMC (p) | Save Cursor (s) | Quit (q): ")
        if input_queue.lower() == "o":
            firefox.open(url)
            print(url)
            input_escape = input("Write? (w)")
            if input_escape.lower() == "w":
                input_mod_link = input("Modified Link?: ")
                if input_mod_link.lower() != "":
                    url = input_mod_link
                chromedriver()
                d0=newspaper4k()
                title = d0["title"]

                #authors = d0["authors"]
                text = d0["text"]
                summarize()
                create_post()

        if input_queue.lower() == "p":
            firefox.open(pmcid)
            print(url)
            input_escape = input("Write? (w)")
            if input_escape.lower() == "w":
                url = pmcid
                chromedriver()
                d0=newspaper4k()
                title = d0["title"]

                #authors = d0["authors"]
                text = d0["text"]
                summarize()
                create_post()


        if input_queue.lower() == "s":
            csm = open("_cursor_save_manual", "a")
            csm.write(dt_string + '[' + cursor + ']' + "\n")
            print("Saved " + cursor)
            csm.close()

        if input_queue.lower() == "q":
            sys.exit()       

    print("completed iterations: " + str(idx+1) + "\n\n")
    meta = cannabinoid_data['meta']
    #print(meta)
    count = cannabinoid_data['meta'].get('count')
    print("RESULTS COUNT:")
    print(count)

# TODO: allow inputting some vars, like starting date. allow saving these dates/progress for continuation.
#       display one result in term (one at a time). query to open doi. query to write post.
#       
