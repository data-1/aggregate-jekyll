import undetected_chromedriver as uc 
import sys
import newspaper
from newspaper import Article
from newspaper import Config
from sumy.utils import get_stop_words
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer
from selenium.webdriver.chrome.options import Options 
import textrazor
import re
from bs4 import BeautifulSoup
import urllib
import json
# ACCEPT URL AS ARG1
#url=sys.argv[1].strip()



url = input("Paste Link:")




script_dir="/home/z0/aggregate-jekyll"
venv_bin_dir="/home/z0/aggregate-jekyll/venv/bin"
jekyll_post_dir="/home/z0/cbdinfo/_posts"
textrazor_api_key="/home/z0/.textrazorapi.key"


# GET TEXTRAZOR API KEY
with open(textrazor_api_key) as file:
    textrazor.api_key = file.read().replace('\n',' ')

def chromedriver():

    # CHROME CONFIG
    options = {
        "profile.default_content_setting_values": {
            "images": 2,
            "javascript": 2,
        }
    }
    options = Options()
    # undetected_chromedriver
    options = uc.ChromeOptions()
    #options.add_argument("--headless=new")

    driver = uc.Chrome(options=options) 
    driver.get(url) 
    page_source = driver.page_source
    #print(driver.current_url)
    #print(driver.title)
    #print(driver.page_source) 

    # WRITE PAGE SOURCE TO _html.html
    x = open("_html.html", "w")
    x.write(page_source)
    x.close()


    driver.quit()

    return page_source


chromedriver()


def get_author_name_url():
    soup = BeautifulSoup(chromedriver(), "html.parser")
    data = json.loads(
        soup.select_one('[type="application/json"]').contents[0]
    )
    return data["abstracts"]["content"][0]["$$"]


print(get_author_name_url())

def newspaper4k():

    # LOAD PAGE SOURCE INTO NEWSPAPER4K
    article = Article(url= 'file:///home/z0/aggregate-jekyll/_html.html')
    article.download()
    article.parse()
    #print(article.ticle.title
    #link = article.url
    #authors = ', '.join(article.authors)
    # clear
    ##open('author.txt', 'w').close()
    # write
    #a = open("author.txt", "w")
    #a.write (authors)
    #a.close()

    #open('title.txt', 'w').close()
    #b = open("title.txt", "w")
    #b.write(title)
    #b.close()

    #return article.text


    y = open('/home/z0/aggregate-jekyll/_html.html', 'r')
    lines = y.readlines()
    # HTML INTO STRING
    html_join_lines = '\t'.join([line.strip() for line in lines])

    
    d = dict([
        ('title',       article.title),
        ('authors',     ', '.join(article.authors)),
        ('url',         url),
        ('text',        article.text)
    ])
    print(article.authors)
    return d

#print ('The returned dictionary is:',create_dictionary())


d0=newspaper4k()
title = d0["title"]
authors = d0["authors"]
url = d0["url"]
text = d0["text"]

def textrazor1():
   
    # CONFIG
    client = textrazor.TextRazor(extractors=["topics"])
    #client.set_cleanup_mode("cleanHTML")
    client.set_classifiers(["textrazor_newscodes"])
    # READ PAGE SOURCE
   # y = open('/home/z0/aggregate-jekyll/_html.html', 'r')
    #lines = y.readlines()
    # HTML INTO STRING
    #html_join_lines = '\t'.join([line.strip() for line in lines])


    response = client.analyze(str(text))
    #seen = set()

    open('extracted_tags.txt', 'w').close()

    topicx = []
    for topic in response.topics():
        if topic.score == 1:
            #print("validate iteration")
            #print(topic.label)
            topicx.append(topic.label)
            
            z = open("extracted_tags.txt", "a")
            z.write(topic.label+"\n")
            z.close()
    return topicx




print(textrazor1())



#print(f'Title: {title}')
#print(f'Link: {link}')
#print(f'Author: {authors}')
#print(clean_summary)



def summarize():
    # SUMMARY OPTIONS
    LANGUAGE = "english"
    SENTENCES_COUNT = 5

    # CLEAN TEXT
    articletext_join_lines = "".join(text.replace("\n", " ").replace('"', ""))
    #print(article.text)
    parser = PlaintextParser.from_string(articletext_join_lines, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    article_summary = []
    for sentence in summarizer(parser.document, SENTENCES_COUNT):
        article_summary.append(str(sentence))

    clean_summary = ' '.join([str(elem) for elem in article_summary])


    open('summary.txt', 'w').close()
    c = open("summary.txt", "w")
    c.write(clean_summary)
    c.close()

summarize()


def create_post():
    pattern0 = '[^a-zA-Z0-9]+'
    #pattern1 = '^-+\|-+$'

   
    print("HERE")
    print(title)
    #print(authors)
    print(url)
    #print(text)
    #
    #mod_url = re.sub(pattern0, title)
    mod_url2 = re.sub(r"[^a-zA-Z0-9]+", ' ', title).strip().casefold().replace(" ", "-")
    #print(re.sub(r"[^a-zA-Z0-9]+", ' ', url))
    print(mod_url2)

    mod_tags = ','.join(textrazor1())
    print(mod_tags)
create_post()


