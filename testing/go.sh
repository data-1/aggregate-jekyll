#!/bin/bash

# CONFIG
script_dir="/home/z0/aggregate-jekyll"
venv_bin_dir="/home/z0/aggregate-jekyll/venv/bin"
jekyll_post_dir="/home/z0/cbdinfo/_posts"
api_key_loc="/home/z0/.textrazorapi.key"

link_input(){
  printf '%s\n' "Paste Link"
  # accept input for specified url
  read -r link
}

newspaper4k(){
  # send link to newspaper3k
  article_data=$("$venv_bin_dir"/python3 "$script_dir"/scrape6.py "$link";)
echo "$article_data"
  Date=$(date '+%Y-%m-%d')

  sed -n '2{s/^$/unknown/;s/^/author: /;p;q;}' author.txt
  Author=$(cat author.txt)
  Title=$(cat title.txt)
  Summary=$(cat summary.txt)

  # output info
  printf 'Title: %s\n\n' "$Title"
  printf 'author: %s\n\n' "$Author"
  printf 'Summary: %s\n\n' "$Summary"
  #printf 'Link: %s\n\n' "$link"
}

textrazor(){
  # trim topics
  extracted_tags=$(tr -dc '[:alnum:]\n\r ()-' < $script_dir/extracted_tags.txt)
  # output result
  printf '%s\n' "keywords:"
  printf '%s\n' "$extracted_tags"
}

create_post(){
  # retrieve category specification from user
  read -rp $'\nEnter Categories (space-separated)\n' categories
  # user verification
  read -rp "Is $categories correct? (Y/y)" -n 1 -r
  # if verified
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    # alphabetise
    sorted_categories=$(echo "$categories" | xargs -n1 | sort | xargs)
    # reformat categories to jekyll format
    mod_categories="$(echo "- ${sorted_categories// /$'\n- '}")"
    printf '%s\n' "categories set to:"
    printf '%s\n' "$mod_categories"
    # create url friendly post name
    mod_url="$(echo -e "${Title}" | iconv -t ascii//TRANSLIT | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr '[:upper:]' '[:lower:]')"
    # generate filename
    mod_filename="$Date-$mod_url"
    printf 'Filename: %s\n' "$mod_filename"
    # generate summary string
    # mod_summary="$(echo -e "#### Summary\\n>$Summary")"
    #printf 'Summary: %s\n' "$mod_summary"
    # generate tags string
    mod_tags="$(echo -e "tags: [$extracted_tags]"  | sed -z 's/\n/,/g;s/,$/\n/')"
    # write post
    echo -e "---\\nlayout: post\\ntitle: \"$Title\"\\ndate: $Date\\ncategories:\\n$mod_categories\\nauthor: $Author\\n$mod_tags\\n---\\n\\n\\n$Summary\\n\\n[Visit Link]($link){:target=\"_blank\" rel=\"noopener\"}\\n\\n" > "$jekyll_post_dir"/"$mod_filename".md
  else
    #if user input!=Yy return to user input
    create_post
  fi
}

main(){
  link_input
  # call newspaper3k function
  newspaper4k
  # call textrazor function
  textrazor
  # call create_post function
  create_post
  main
}

main
