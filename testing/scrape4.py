from newspaper import Config
from newspaper import Article

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0'

config = Config()
config.browser_user_agent = USER_AGENT
config.request_timeout = 10

base_url = 'https://www.sciencedirect.com/science/article/pii/S2405654524000052'
article = Article(base_url, config=config)
article.download()
article.parse()
article.nlp()
print(article.summary)
