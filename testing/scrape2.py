import sys
import textrazor
from newspaper import Article
from newspaper import Config
from sumy.utils import get_stop_words
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer

url=sys.argv[1].strip()

LANGUAGE = "english"

# configurable number of sentences
SENTENCES_COUNT = 5




USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0'

config = Config()
config.browser_user_agent = USER_AGENT
config.request_timeout = 10

article = Article(url, config=config)
article.download()
article.parse()



# text cleaning
text = "".join(article.text).replace("\n", " ").replace('"', "")
parser = PlaintextParser.from_string(text, Tokenizer(LANGUAGE))
stemmer = Stemmer(LANGUAGE)

summarizer = Summarizer(stemmer)
summarizer.stop_words = get_stop_words(LANGUAGE)

article_summary = []
for sentence in summarizer(parser.document, SENTENCES_COUNT):
    article_summary.append(str(sentence))

clean_summary = ' '.join([str(elem) for elem in article_summary])

title = article.title

link = article.url

authors = ', '.join(article.authors)


# GET CATEGORIES
textrazor.api_key = "4de2c821d6b44312ee48befb5f3a2e3d76ee55a32307626b51f73b16"

client = textrazor.TextRazor(extractors=["topics"])
client.set_cleanup_mode("cleanHTML")
client.set_classifiers(["textrazor_newscodes"])
client.set_download_user_agent('Mozilla/5.0 (Windows NT 10.0; rv:125.0) Gecko/20100101 Firefox/125.0')
response = client.analyze_url(url)

seen = set()
#topics = ' '.join(response.authors)
#print (entities)

open('extracted_tags.txt', 'w').close()
for topic in response.topics():
  if topic.score == 1:
    print (topic.label + "\n")
    # write to file
    z = open("extracted_tags.txt", "a")
    z.write(topic.label+"\n")
    z.close()


#print(f'Title: {title}')
#print(f'Link: {link}')
#print(f'Author: {authors}')
#print(clean_summary)

# write to file
a = open("author.txt", "w")
#a.write(authors + "\n" + title +"\n" + summary)
a.write (authors)
a.close()

b = open("title.txt", "w")
b.write(title)
b.close()

c = open("summary.txt", "w")
c.write(clean_summary)
c.close()


