import undetected_chromedriver as uc 
import sys
import newspaper
from newspaper import Article
from newspaper import Config
from sumy.utils import get_stop_words
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer
from selenium.webdriver.chrome.options import Options 
import textrazor
import re
import codecs
# ACCEPT URL AS ARG1
url=sys.argv[1].strip()

# CHROME CONFIG
chrome_prefs = {
    "profile.default_content_setting_values": {
        "images": 2,
        "javascript": 2,
    }
}
options = Options()
# undetected_chromedriver
options = uc.ChromeOptions()
#options.add_argument("--headless=new")

driver = uc.Chrome(options=options) 
driver.get(url) 
page_source = driver.page_source

#print(driver.current_url)
#print(driver.title)
#print(driver.page_source) 

sourceFileName="_html.html"
targetFileName="html_.html"
BLOCKSIZE = 1048576 # or some other, desired size in bytes
with codecs.open(sourceFileName, "r") as sourceFile:
    with codecs.open(targetFileName, "w", "utf-8") as targetFile:
        while True:
            contents = sourceFile.read(BLOCKSIZE)
            if not contents:
                break
            targetFile.write(contents)




# WRITE PAGE SOURCE TO _html.html
x = open("_html.html", "w")
x.write(page_source)
x.close()

driver.quit()

# LOAD PAGE SOURCE INTO NEWSPAPER4K
article = Article(url= 'file:///home/z0/aggregate-jekyll/_html.html')
article.download()
article.parse()
#print(article.text)

# GET TEXTRAZOR API KEY
with open("/home/z0/.textrazorapi.key") as file:
    textrazor.api_key = file.read().replace('\n',' ')

# CONFIG
client = textrazor.TextRazor(extractors=["topics"])
client.set_cleanup_mode("cleanHTML")
client.set_classifiers(["textrazor_newscodes"])
# READ PAGE SOURCE
#y = open('/home/z0/aggregate-jekyll/_html.html', 'r')
#lines = y.readlines()
# HTML INTO STRING
#html_join_lines = '\t'.join([line.strip() for line in lines])
articletext_join_lines = "".join(str(article.text)).replace("\n", " ")
#articletext_join_lines = str(article.text)

print(articletext_join_lines)
#no_punc_string = re.sub(r'[^\w\s\n]','', articletext_join_lines) 

#print(no_punc_string)

response = client.analyze(articletext_join_lines)

print(response.error)
print(response.message)
json_response = response.json
print(json_response)
#print(articletext_join_lines)
seen = set()

# CLEAR extracted_tags.txt
open('extracted_tags.txt', 'w').close()
# for each label with score of 1, write to extracted_tags.txt
for topic in response.topics():
  if topic.score == 0.5:
    # write to file
    z = open("extracted_tags.txt", "a")
    z.write(topic.label+"\n")
    z.close()
    print(topic.label)
#print(f'Title: {title}')
#print(f'Link: {link}')
#print(f'Author: {authors}')
#print(clean_summary)


# SUMMARY OPTIONS
LANGUAGE = "english"
SENTENCES_COUNT = 5
# CLEAN TEXT
#print(article.text)
parser = PlaintextParser.from_string(articletext_join_lines, Tokenizer(LANGUAGE))
stemmer = Stemmer(LANGUAGE)
summarizer = Summarizer(stemmer)
summarizer.stop_words = get_stop_words(LANGUAGE)

article_summary = []
for sentence in summarizer(parser.document, SENTENCES_COUNT):
  article_summary.append(str(sentence))

clean_summary = ' '.join([str(elem) for elem in article_summary])
title = article.title
link = article.url
authors = ', '.join(article.authors)

# clear
open('author.txt', 'w').close()
# write
a = open("author.txt", "w")
a.write (authors)
a.close()

open('title.txt', 'w').close()
b = open("title.txt", "w")
b.write(title)
b.close()

open('summary.txt', 'w').close()
c = open("summary.txt", "w")
c.write(clean_summary)
c.close()
