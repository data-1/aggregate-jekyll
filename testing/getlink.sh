#!/bin/bash
selection="/home/z0/cbdinfo/_posts/*.md"
output="_link_list"
:> "$output"
for md in $selection; do
  result=$(grep -o -P '(?<=Visit Link]\().*(?=\)\{:target)' "$md")
  echo "$result" >> "$output"
done; sed -i '/^$/d' "$output"
cat "$output"
