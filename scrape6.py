import undetected_chromedriver as uc
import newspaper
from newspaper import Article
#from newspaper import Config
from sumy.utils import get_stop_words
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import textrazor
import re
import sys
import datetime
from selenium import webdriver
import nltk
import nltk
nltk.download('punkt_tab')

script_dir="/home/z0/aggregate-jekyll"
venv_bin_dir="/home/z0/aggregate-jekyll/venv/bin"
jekyll_post_dir="/home/z0/cbdinfo/_posts"
textrazor_api_key="/home/z0/.textrazorapi.key"

# GET TEXTRAZOR API KEY
with open(textrazor_api_key) as file:
    textrazor.api_key = file.read().replace('\n',' ')

def chromedriver():
    # CHROME CONFIG
    options = {
        "profile.default_content_setting_values": {
            "images": 2,
            "javascript": 2,
        }
    }
    options = Options()

    #options.add_extension('SingleFile_1_22_49_0.crx')
   # options.page_load_strategy = 'none'

    # undetected_chromedriver
    #options = uc.ChromeOptions()
    #options.add_argument("--headless=new")
    #driver = uc.Chrome(options=options) 
    driver = uc.Chrome(options=options) 

    driver.get(url)
    #element = driver.find_element_by_name('body')
    #element.send_keys(Keys.CONTROL+Keys.SHIFT+"Y")
    page_source = driver.page_source
    #print(driver.current_url)
    #print(driver.title)
    #print(driver.page_source) 
    # WRITE PAGE SOURCE TO _html.html
    x = open("_html.html", "w")
    x.write(page_source)
    x.close()
    driver.quit()
    #return page_source

def newspaper4k():
    # LOAD PAGE SOURCE INTO NEWSPAPER4K
    article = newspaper.article(url= 'file:///home/z0/aggregate-jekyll/_html.html')
    #article = newspaper.article('https://edition.cnn.com/2023/10/29/sport/nfl-week-8-how-to-watch-spt-intl/index.html')
    article.download()
    article.parse()
    
    d = dict([
        ('title',       article.title),
        ('authors',     ' | '.join(article.authors)),
        ('url',         url),
        ('text',        article.text)
    ])
    #print(article.authors)
    return d

def textrazor1():
    # CONFIG
    client = textrazor.TextRazor(extractors=["topics"])
    #client.set_cleanup_mode("cleanHTML")
    client.set_classifiers(["textrazor_newscodes"])
    response = client.analyze(str(text))

    topicx = []
    for topic in response.topics():
        if topic.score == 1:
            #print("validate iteration")
            #print(topic.label)
            topicx.append(topic.label)
    return topicx
#print(textrazor1())

def summarize():
    # SUMMARY OPTIONS
    LANGUAGE = "english"
    SENTENCES_COUNT = 5
    #nltk.download('punkt') 
    # CLEAN TEXT
    articletext_join_lines = "".join(text.replace("\n", " ").replace('"', ""))
    #print(article.text)
    parser = PlaintextParser.from_string(articletext_join_lines, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    article_summary = []
    for sentence in summarizer(parser.document, SENTENCES_COUNT):
        article_summary.append(str(sentence))

    clean_summary = ' '.join([str(elem) for elem in article_summary])
    return clean_summary

def create_post():
    print(title)
    #print(authors)
    #print(url)
    #print(text)
    mod_url2 = re.sub(r"[^a-zA-Z0-9]+", ' ', title).strip().casefold().replace(" ", "-")
    mod_tags = ','.join(textrazor1())
    datex = datetime.datetime.now()
    datex_fn = datex.strftime("%Y-%m-%d")
    mod_filename = ''.join((datex_fn, "-", mod_url2))
    _post = ''.join((jekyll_post_dir, '/', mod_filename, '.md'))
    
    input("Write post?")

    summary = summarize()
    _post_content = ''.join(("---\nlayout: post\ntitle: \"", title, "\"\ndate: ", datex_fn, "\ntags: [",  mod_tags, "]\nauthor: ", authors, "\n---\n\n\n", summary, "\n\n[Visit Link](", url, ")", "{:target=\"_blank rel=\"noopener\"}\n\n"))

    open(_post, 'w').close()
    d = open(_post, "w")
    d.write(_post_content)
    d.close()

def qcontinue():
    #cont = input("Continue? (Y/y)")
    #if cont in ('Y', 'y'):
    #    main()
    #else:
    #    sys.exit()
    main()

def main():
    #input link
    global url
    url = input("Paste Link:")
    chromedriver()
    global d0
    global title
    global authors
    #global url
    global text
    d0=newspaper4k()
    title = d0["title"]
    authors = d0["authors"]
    text = d0["text"]
    summarize()
    create_post()
    # ACCEPT URL AS ARG1
    #url=sys.argv[1].strip()
    qcontinue()


main()
